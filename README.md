# Study Project - Lot sizing Optimization

This work includes a basic model of lot sizing and sequencing, the
Proportional Lotsizing Scheduling Problem (PLSP), which is linked to important factors concerning
energy and emissions in production. The result is
a model extension PLSP (Energy Storage with Emissions consideration)-ESE
is presented. By taking into account the aspects of own energy production, energy storage, connection to the energy market and trading with energy, the model can be extended,
connection to the energy market and trading with emission certificates,
a cost- and energy-efficient production plan is created. The entire work is implemented in GAMS. 


The gms-file can be found under the name PLSPESS.gms in the folder model. 

As this project was done as part of my bachelor studies, the model as well as the report are in German. However, the truly interesting part of this project, meaning the model extencion, got translated in English and can be found under the name Mathematical_Model_Chapter_of_Lotsizing_Optimization.pdf. 
