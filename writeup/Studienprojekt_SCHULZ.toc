\babel@toc {ngerman}{}
\contentsline {section}{Abbildungsverzeichnis}{iv}{section*.2}%
\contentsline {section}{Tabellenverzeichnis}{iv}{section*.3}%
\contentsline {section}{Abkürzungsverzeichnis}{iv}{section*.4}%
\contentsline {section}{Symbolverzeichnis}{v}{section*.6}%
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}%
\contentsline {section}{\numberline {2}Grundlegende Aspekte von energieorientierter Losgrößen- und Reihenfolgeplanung unter Berücksichtigung von Emissionen}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Einordnung der Losgrößen- und Reihenfolgeplanung in die Produktionsplanung}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Rolle von Energie und Emissionen}{5}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Literatur und Klassifizierung relevanter mathematischer Modelle}{6}{subsection.2.3}%
\contentsline {section}{\numberline {3}Proportional Lotsizing Scheduling Problem (PLSP) mit Energiespeicher unter Emissionsrestriktionen (PLSP-ESE)}{10}{section.3}%
\contentsline {subsection}{\numberline {3.1}Formulierung des Grundmodells PLSP}{10}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Modellannahmen}{10}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Mathematische Modellformulierung}{12}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Modellerweiterung zum PLSP-ESE}{13}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Modellannahmen der Erweiterung}{13}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Maschinenzustand}{15}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}Energienachfrage}{15}{subsubsection.3.2.3}%
\contentsline {subsubsection}{\numberline {3.2.4}Wetter- und Zeitabhängigkeit}{15}{subsubsection.3.2.4}%
\contentsline {subsubsection}{\numberline {3.2.5}Technische Limitierung von Batterien}{16}{subsubsection.3.2.5}%
\contentsline {subsubsection}{\numberline {3.2.6}Auf- und Entladeprozess}{16}{subsubsection.3.2.6}%
\contentsline {subsubsection}{\numberline {3.2.7}Verbindung zum öffentlichen Stromnetz}{16}{subsubsection.3.2.7}%
\contentsline {subsubsection}{\numberline {3.2.8}Emissionsbeschränkungen}{17}{subsubsection.3.2.8}%
\contentsline {subsection}{\numberline {3.3}Mathematische Modellformulierung PLSP-ESE}{18}{subsection.3.3}%
\contentsline {section}{\numberline {4}Numerischer Studie zum Vergleich der Modelle PLSP, PLSP-ESE und dem energieorientierten PLSP (PLSP-EC)}{22}{section.4}%
\contentsline {subsection}{\numberline {4.1}Generierung der Testinstanzen}{22}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Ergebnisse und Analyse der numerischen Studie}{24}{subsection.4.2}%
\contentsline {section}{\numberline {5}Betriebswirtschaftliche Bewertung und Ausblick}{31}{section.5}%
\contentsline {section}{Literatur}{33}{section*.23}%
\contentsline {section}{\numberline {A}Modellierungen und Implementierungen der Modelle}{37}{appendix.A}%
\contentsline {subsection}{\numberline {A.1}Ergänzungen zur numerischen Studie}{37}{subsection.A.1}%
\contentsline {subsubsection}{\numberline {A.1.1}Mathematische Modellformulierung des PLSP mit Energy Consideration (PLSPEC)}{37}{subsubsection.A.1.1}%
\contentsline {subsubsection}{\numberline {A.1.2}Energiekostenrechnung PLSP}{40}{subsubsection.A.1.2}%
\contentsline {subsubsection}{\numberline {A.1.3}Weitere Daten für die Testinstanzen}{40}{subsubsection.A.1.3}%
\contentsline {subsubsection}{\numberline {A.1.4}Vollständiger Überblick der Ergebnisse der numerischen Studie}{41}{subsubsection.A.1.4}%
\contentsline {subsection}{\numberline {A.2}Implementierungen in GAMS}{43}{subsection.A.2}%
\contentsline {subsubsection}{\numberline {A.2.1}PLSP-ESE}{43}{subsubsection.A.2.1}%
\contentsline {subsubsection}{\numberline {A.2.2}PLSP-EC}{55}{subsubsection.A.2.2}%
\contentsline {subsubsection}{\numberline {A.2.3}PLSP}{63}{subsubsection.A.2.3}%
