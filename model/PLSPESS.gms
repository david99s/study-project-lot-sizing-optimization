$title PLSPESS
*Proportional Lotsizing Scheduling Problem

Sets
t Zeitperioden
j Produkte

esmax_Fall  looping-Set fuer Groeße der Batterie (einfluss auf bzw. variiert esmax)     
qpv_Fall    looping-Set fuer Leistung der PV-Anlage (variiert maximale leistung der PV-Anlage q_pv)
emax_Fall   looping-Set fuer Menge an straffreien Emissionen (variiert emax_t)
ed_Fall looping-Set fuer energiebedarf bei herstellung der güter
;
sets
t /t1*t24/
j /j1*j3/
ep /ep1*ep3/

esmax_Fall /esmax_Fall1*esmax_Fall3/
qpv_Fall /qpv_Fall1*qpv_Fall3/
emax_Fall /emax_Fall1*emax_Fall3/
ed_Fall /ed_Fall1*ed_Fall2/;


parameter

C(t) zeitliche Kapazität von Maschine in Periode t
d(j,t) Nachfrage von Produkt j in Periode t
h(j) Lagerkosten von Produkt j
p(j) Zeit für Produktion von Produkt j
p2(j) Zeit für Setup von Maschine für Produkt j
s(j) Setup Kosten für Produkt j
B big number

co2tax Kostenfaktor bei Verkauf von Emissionen 
ed(j) Energieverbrauch druch herstellung von Produkt j in Periode t in kwh
edcase(j,ed_Fall) Variation von edj
edidle(j) Energieverbrauch im Maschinenzustand idle 
edsetup(j) Energieverbrauch im Maschinenzustand setup
esmax Speicherkapazität in verschiedenen Variationen
esmaxc(esmax_Fall) Variationskoeffizient der Batteriegröße
w(t) Wetterdaten gibt an ob in t Sonne scheint oder nicht
ecsell(t) Energiepreis im Netz für Energie bei Verkauf in Periode t
ecbuy(ep,t) Energiepreis im Netz für Energie bei Zukauf in Periode t abhängig von Anbieter in EuroKwH
qpv Menge an Energie die durch PV Anlage in einer Stunde produziert wird in Variationen
qpvc(qpv_Fall) Variationskoeffizient der Leistung der PV-Anlage
qcmax - technische limitierung von aufladen
qdcmax - technische Limitierung von entladen
gmax - technische Limitierung abhängig von Energieanbieter
co2mix(ep) - Co2 Emissionen in Gramm pro Kwh von zugekauften Energiemix von Energieprovider ep 

n Wirkungsgrad Batterie
energyq(ep,t) Menge an EnergieStrom die die Energieprovider abhängig von Zeit zur Verfügung haben
emax(t) Menge an maximalen Co2 Emissionen die durch in Produktion eingesetzte Energie verursacht werden dürfen pro Zeiteinheit t
emaxc(t,emax_Fall) Variationskoeffizient für erlaubte Emissionen
Ied benötigte Energie pro einer Produkteinheit in Lager in kwh
co2penalty Strafkosten pro Co2 Emissionsoverload in Euro pro Gramm

resultsZF(esmax_Fall, qpv_Fall, ed_Fall, emax_Fall) Zielfunktionswerte abhänging von Instanz
resultsProdMenge(j,t,esmax_Fall, qpv_Fall, ed_Fall, emax_Fall) Produktionsmengen abhänging von Instanz
;


Positive Variables
x(j,t) Entscheidungsvariable die bestimmt ob Setup für Produkt j in Periode t stattfindet oder nicht
I(j,t) Lagerbestand von Produkt j in Periode t
q(j,t) Anzahl produzierte Einheiten von Produkt j in Periode t
qp(t) Energiemenge die direkt in Produktion fließt
qsell(t) Energiemenge die in t von produzierter PV-Energie verkauft wird
qselles(t) Energiemenge die in t aus dem Speicher verkauft wird
qsellges(t) Gesamtmenge die in t verkauft wird
qc(t) Menge an Energie die in t von PV geladen wird
qcges(t) Menge an Energie die in t insgesamt in Speicher geladen wurde
qdc(t) Menge an Energie die aus Speicher für Produktion verwendet wurde in Periode t in kwh
qdcges(t) Menge an Energie die in t insgesamt aus Speicher geflossen ist
es(t) Energiemenge in Energiespeicher zum Zeitpunkt t
qbuy(ep,t) Menge an Energie die in Periode t aus dem Netz von Energieprovider ep zur Produktion zugekauft wurde
qbuyes(ep,t) Menge an Energie die in Periode t aus dem Netz von Energieversorger ep zum Speichern zugekauft wurde
qbuyges(ep,t) Gesamtmenge an Energie die in Periode t aus Netz zugekauft wird
qstrom(t) Gesamtmenge an benötigter Energie für Produktion in t
idlet(t) Zeit die Maschine im Leerlauf ist in Periode t
co2q(t) Menge an co2 Emission in Periode t
Ico2(t) imaginärer Lagerbestand von co2 Emissionen um gesparte Emissionen in zukünftigen Perioden verwenden zu können und trotzdem das Emissionsziel einzuhalten
Ico2Final überschüssige co2 Emissionen am Ende der letzten Periode
co2overload(t) benötigte Co2 Emissionen zusätzlich zur Emissionsgrenze
qemtotal Gesamtmenge an benötigten Emissionen für Produktion
;



I.up(j,t)$(ord(t)=24)=0;

Binary Variables 

y(j,t) Binärvariable die bestimmt ob Maschinensetup für Produkt j am Ende von Periode t stattgefunden hat oder nicht
alpha(t) Binärvariable die bestimmt ob Speicher aufgeladen oder entladen wird

delta(t) Binärvariable die entscheidet ob Strom aus Netz zugekauft oder verkauft wird;

y.up(j,t)$(ord(t)=0)=0;

Variables
ZF Zielfunktionswert;

Equations
Zfkt Zielfunktion
lbilanz(j,t) Lagerbilanzgleichung
stcheck(t) stellt sicher dass immer am Ende von t die Maschine für ein bestimmtes Produkt eingestellt ist
stcon(j,t) stellt Verbindung zwischen setup variable x und setupcheck variable y her
prodcheck(j,t) stellt sicher dass niemals ohne den entsprechenden Setup status für Produkt j produziert werden kann
kapres(t) Zeitliche Kapazitätsrestriktion für Maschine pro Periode
strom(t) gesamtstrombedarf in Periode t für Produktion t
speicherkap(t) Kapazitätrestriktion von Batterie 
stromverteilung(t) Verteilung der in Periode t produzierten Energie auf Nachfrage Speicher und Verkauf
stromdemand(j,t) aktuelle Stromnachfrage muss durch Kombination von STromproduktion Strom aus Batterie und Zukauf mindestens gedeckt sein
strombilanz(ep,t) Gespeicherte Menge in t entspricht exakt Speicher aus t-1 sowie Differenz aus Zufluss und Nutzung 

chargeKap(t) technische Limitierung von Aufladeprozess pro Periode
chargeges(t) bla
dischargeKap(t) technische Limitierung von Entladeprozess pro Periode
dischargeges(t) bla
buyKaptech(ep,t) technische Limitierung der Leitung von Zukauf aus Netz pro Periode
buyges(ep,t) Gesamtkauf setzt sich aus Kauf für produktion und kauf für speicher zusammen
sellKap(ep,t) technische Limitierung der Leitung bei verkauf an Netz pro Periode
sellges(t) GEsamtverkauf setzt sich aus Verkauf von PV und Speicher zsm
idleTime(t) Zeit die Maschinenzustand idle ist in Periode t
co2(t) Gleichung für Menge an Emissionen durch Zukauf von Energie

co2capcum(t) Lagergleichung von Co2 Emissionen sodass unbenutzte Emissionen später benutzt werden können
Ico2finalzuweisung Endsumme an Emissionslagerbestand der dann verkauft werden kann
finalqem Endsumme von insgesamt benötigten Emissionen
;



Zfkt .. 
        ZF=e= sum((j,t), s(j)*x(j,t) + h(j)*I(j,t)) + sum((ep,t), (qbuyges(ep,t)*ecbuy(ep,t)- qsellges(t)*ecsell(t)))- Ico2final*co2tax + sum(t, co2overload(t))*co2penalty;
        
lbilanz(j,t)$(ord(t)>0) ..
        I(j,t-1)$(ord(t)>0) + q(j,t) - I(j,t)$(ord(t)) =e= d(j,t);
        
stcheck(t) ..
        sum(j, y(j,t)) =l= 1;
        
stcon(j,t)$(ord(t)>0)..
        x(j,t) - y(j,t)$(ord(t)) + y(j,t-1)$(ord(t)>0) =g= 0;
        
prodcheck(j,t)$(ord(t)>0)..
        B*y(j,t)$(ord(t)) + B * y(j,t-1)$(ord(t)>0) - q(j,t) =g= 0;
        
kapres(t)..
        sum((j), p(j)*q(j,t) + x(j,t)*p2(j)) =l= C(t);
        
strom(t)..
        sum(j, (p(j)*q(j,t)/C(t))*ed(j) + (idlet(t)/C(t))*edidle(j) + ((x(j,t)*p2(j))/C(t))*edsetup(j) + Ied*I(j,t)) =e= qstrom(t);
        
speicherkap(t)..
        es(t) =l= esmax;
        
stromverteilung(t)..
        w(t)*qpv =e= qp(t) + qc(t) + qsell(t);
        
stromdemand(j,t)..
        qstrom(t) =l= qp(t) + qdc(t) + sum(ep,qbuy(ep,t));
        
strombilanz(ep,t)..
        es(t) =e= es(t-1)$(ord(t)>0) + n*qcges(t) - qdcges(t) ;
        
chargeKap(t)..
        qcges(t) =l= qcmax * alpha(t);
        
chargeges(t)..
        qcges(t) =e= qc(t) + sum(ep,qbuyes(ep,t));
        
dischargeKap(t)..
        qdcges(t) =l= qdcmax*(1-alpha(t));
        
dischargeges(t)..
        qdcges(t) =e= qdc(t) + qselles(t);
        
buyKaptech(ep,t)..
        qbuyges(ep,t) =l= energyq(ep,t) * delta(t);

buyges(ep,t)..
        qbuyges(ep,t) =e= qbuy(ep,t) + qbuyes(ep,t);
        
sellKap(ep,t)..
        qsellges(t) =l= gmax*(1-delta(t));

sellges(t)..
        qsellges(t) =e= qsell(t) + qselles(t);
        
idleTime(t)..
        idlet(t) =e= C(t) - sum((j), p(j) * q(j,t) + x(j,t)*p2(j));
        
co2(t)..
        sum((ep),qbuyges(ep,t)*co2mix(ep))=e= co2q(t);
  

        
co2capcum(t)..
        Ico2(t) =e= Ico2(t-1)$(ord(t)>0) - (sum(ep,(co2mix(ep)*qbuyges(ep,t))) - emax(t)) + co2overload(t);

Ico2finalzuweisung..
        Ico2final =e= (sum(t, Ico2(t)) - sum(t$((ord(t)>0) and (ord(t)<card(t))), Ico2(t)));
        
finalqem..
        qemtotal =e= sum(t, co2q(t));
        
parameter

h(j) /j1 2.0, j2 1.5, j3 1/
s(j) /j1 400, j2 150, j3 300/
p(j) /j1 1, j2 1, j3 1/
p2(j) /j1 0.5, j2 0.5, j3 0.5/
C(t) /t1 60, t2 60,t3 60,t4 60,t5 60,t6 60,t7 60,t8 60,t9 60,t10 60, t11 60, t12 60,t13 60,t14 60,t15 60,t16 60,t17 60,t18 60,t19 60,t20 60,t21 60,t22 60,t23 60,t24 60/

edidle(j) /j1 10, j2 10, j3 10/
edsetup(j) /j1 200, j2 250,j3 300/

ecsell(t) /t1 0.026,t2 0.027,t3 0.027,t4 0.029,t5 0.031,t6 0.035,t7 0.044,t8 0.052,t9 0.055,t10 0.048,t11 0.047,t12 0.037,t13 0.032,t14 0.029,t15 0.029,t16 0.031,t17 0.040,t18 0.046,t19 0.050,t20 0.051,t21 0.049,t22 0.045,t23 0.039,t24 0.039/

co2mix(ep) /ep1 10, ep2 150, ep3 400/
esmaxc(esmax_Fall) /esmax_Fall1 0, esmax_Fall2 200, esmax_Fall3 1000/
qpvc(qpv_Fall) /qpv_Fall1 0, qpv_Fall2 500, qpv_Fall3 1000/

emaxc(t,emax_Fall) /t1.emax_Fall1 100, t2.emax_Fall1 100,t3.emax_Fall1 100,t4.emax_Fall1 100,t5.emax_Fall1 100,t6.emax_Fall1 100,t7.emax_Fall1 100,t8.emax_Fall1 100,t9.emax_Fall1 100,t10.emax_Fall1 100,
t11.emax_Fall1 100, t12.emax_Fall1 100,t13.emax_Fall1 100,t14.emax_Fall1 100,t15.emax_Fall1 100,t16.emax_Fall1 100,t17.emax_Fall1 100,t18.emax_Fall1 100,t19.emax_Fall1 100,t20.emax_Fall1 100,t21.emax_Fall1 100,t22.emax_Fall1 100,t23.emax_Fall1 100,t24.emax_Fall1 100, t1.emax_Fall2 250, t2.emax_Fall2 250,t3.emax_Fall2 250,t4.emax_Fall2 250,t5.emax_Fall2 250,t6.emax_Fall2 250,t7.emax_Fall2 250,t8.emax_Fall2 250,t9.emax_Fall2 250,t10.emax_Fall2 250,
t11.emax_Fall2 250, t12.emax_Fall2 250,t13.emax_Fall2 250,t14.emax_Fall2 250,t15.emax_Fall2 250,t16.emax_Fall2 250,t17.emax_Fall2 250,t18.emax_Fall2 250,t19.emax_Fall2 250,t20.emax_Fall2 250,t21.emax_Fall2 250,t22.emax_Fall2 250,t23.emax_Fall2 250,t24.emax_Fall2 250,
t1.emax_Fall3 500, t2.emax_Fall3 500,t3.emax_Fall3 500,t4.emax_Fall3 500,t5.emax_Fall3 500,t6.emax_Fall3 500,t7.emax_Fall3 500,t8.emax_Fall3 500,t9.emax_Fall3 500,t10.emax_Fall3 500, t11.emax_Fall3 500, t12.emax_Fall3 500,t13.emax_Fall3 500,t14.emax_Fall3 500,t15.emax_Fall3 500,t16.emax_Fall3 500,t17.emax_Fall3 500,t18.emax_Fall3 500,t19.emax_Fall3 500,t20.emax_Fall3 500, t21.emax_Fall3 500, t22.emax_Fall3 500, t23.emax_Fall3 500, t24.emax_Fall3 500/

edcase(j,ed_Fall) /j1.ed_Fall1 100, j2.ed_Fall1 100, j3.ed_Fall1 100, j1.ed_Fall2 300, j2.ed_Fall2 600, j3.ed_Fall2 500/
w(t) /t1 0, t2 0,t3 0,t4 0,t5 0,t6 0.3,t7 0.4,t8 0.5,t9 0.5,t10 0.8, t11 0.9, t12 1,t13 1,t14 1,t15 0.9,t16 0.8,t17 0.8,t18 0.7,t19 0.6,t20 0.4,t21 0.3,t22 0.1,t23 0,t24 0/;

gmax=500;
B = 1000;
co2tax=0.02;
qcmax = 500;
qdcmax=500;
Ied=1;
co2penalty=0.05;
n=0.95;


table d(j,t)
    t1  t2  t3  t4  t5  t6  t7  t8  t9  t10  t11  t12  t13  t14  t15  t16  t17  t18  t19  t20  t21  t22  t23  t24 

j1  0   30  0   0   0   80  0   0   40   0   20   0    0    35   60   40   0    0    0    0    10   5    0    0
j2 20    0  0   0   0   60  0   0   90   0   0    20   30   0    0    20   0    0    20   10   30   0    50   50
j3  0    0  0   0   0   40  0   0   60   10  0    40   0    10   0    0    35  50    20   60   0    70   10  10;
   
table ecbuy(ep,t)
    t1    t2    t3    t4     t5    t6    t7    t8    t9    t10   t11   t12   t13   t14   t15   t16   t17   t18   t19   t20   t21   t22   t23   t24

ep1 0.046 0.047 0.047 0.049 0.051 0.055 0.064 0.072 0.075 0.068 0.067 0.057 0.052 0.049 0.049 0.051 0.060 0.066 0.070 0.071 0.069 0.065 0.059 0.059     
ep2 0.056 0.057 0.057 0.059 0.061 0.065 0.074 0.082 0.085 0.078 0.077 0.067 0.062 0.059 0.059 0.061 0.070 0.076 0.080 0.081 0.079 0.075 0.069 0.069  
ep3 0.066 0.067 0.067 0.069 0.071 0.075 0.084 0.092 0.095 0.088 0.087 0.077 0.072 0.069 0.069 0.071 0.080 0.086 0.090 0.091 0.089 0.085 0.079 0.079;

table energyq(ep,t)
    t1  t2  t3  t4  t5  t6  t7  t8  t9  t10 t11 t12 t13 t14 t15 t16 t17 t18 t19 t20 t21 t22 t23 t24  

ep1 0   0   5   10   0  20  60  100 120 200 350 450 500 500 450 420 360 230 150 100 40  10  0   0  
ep2 70  100 120 100 120 230 300 350 400 400 450 500 500 500 470 460 400 350 300 200 140 100 50  50
ep3 500 500 500 500 500 500 500 500 500 500 500 500 500 500 500 500 500 500 500 500 500 500 500 500;


Model PLSPESS / all /;

loop(esmax_Fall,
    loop(qpv_Fall,
        loop(ed_Fall,
            loop(emax_Fall,
            qpv = qpvc(qpv_Fall);
            esmax = esmaxc(esmax_Fall);
            ed(j) = edcase(j,ed_Fall);
            emax(t) = emaxc(t,emax_Fall);
                solve PLSPESS minimizing ZF using mip;

                display q.l, qemtotal.l;
                
                resultsZF(esmax_Fall, qpv_Fall, ed_Fall, emax_Fall) = ZF.l;

                resultsProdMenge(j,t,esmax_Fall, qpv_Fall, ed_Fall, emax_Fall) = q.l(j,t);
            )
        
        )
    )
)

execute_unload "resultskombiniertPLSPESS.gdx" resultsZF, resultsProdMenge
execute 'gdxxrw.exe resultskombiniertPLSPESS.gdx output=resultskombiniertPLSPESS.xlsx par=resultsZF rng=Tabelle1!a1 par=resultsProdMenge rng=Tabelle2!a1';

